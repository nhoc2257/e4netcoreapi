﻿using E4NetCoreApi.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
   options) : base(options)
    {
    }
    public DbSet<Product> Products { get; set; }
}