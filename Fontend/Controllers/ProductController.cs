﻿using Microsoft.AspNetCore.Mvc;

namespace Frontend.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Display(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        public IActionResult Update(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public IActionResult Delete(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}
